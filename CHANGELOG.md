# Douane installer Changelog

## Unreleased



# v0.8.2 - 2020-04-20

 * Adds a VERSION file
 * Populate the installer.VERSION from the VERSION file
 * Adds a --version flag that takes the version from VERSION
 * Adds a --tag flag to set the VERSION to be installed
 * Downloads given version instead of master 
