import logging


from douane.dependencies_manager_factory import DependenciesManagerFactory
from douane.package_manager_factory import PackageManagerFactory

class DependenciesInstaller:
    def __init__(self, on_failure, configuration):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.on_failure = on_failure
        self.configuration = configuration

    def install(self):
        self.logger.debug('install ...')

        package_manager = self.__initialise_package_manager()
        package_manager.on_failure(self.on_failure)

        dependencies_manager = self.__initialise_dependencies_manager()
        packages = dependencies_manager.get_package_list()
        packages.sort()

        return package_manager.install(packages)

    def __initialise_dependencies_manager(self):
        factory = DependenciesManagerFactory()

        return factory.create(self.configuration.distribution[0],
                              self.configuration)

    def __initialise_package_manager(self):
        factory = PackageManagerFactory()

        return factory.create(self.configuration.package_format)
