import logging

from douane.actions import InstallAction, UninstallAction
from douane_installer_event_broadcaster import DouaneInstallerEventBroadcaster
from event_broadcaster_states import EventBroadcasterStates

class DouaneInstaller:
    """
    This class is responsible to execute the download, compilation
    and installation of Douane, and also to update the progress bar.
    """
    def __init__(self, configuration):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.configuration = configuration
        self.event_broadcaster = DouaneInstallerEventBroadcaster(
            configuration=self.configuration
        )
        self.event_broadcaster.connect()

    def run(self):
        if self.configuration.uninstall:
            self.__uninstall()
        else:
            self.__install()

    def __install(self):
        InstallAction(configuration=self.configuration,
                      on_failure=self.__emit_action_failed,
                      on_autostart_dialog=self.__emit_autostart_douane_dialog,
                      on_success=self.__emit_action_succeeded,
                      on_update=self.__emit_update_state).run()

    def __uninstall(self):
        UninstallAction(configuration=self.configuration,
                        on_failure=self.__emit_action_failed,
                        on_success=self.__emit_action_succeeded,
                        on_update=self.__emit_update_state).run()

    def __emit_action_failed(self, error):
        self.logger.debug(f'[emit_install_failed] error: {error}')
        self.logger.debug(f'[emit_install_failed] Sending error through the '
                          'pipe ...')
        self.logger.error(error)
        self.event_broadcaster.broadcast(
            event=EventBroadcasterStates.FAILURE,
            args={
                'error': error
            }
        )

    def __emit_action_succeeded(self):
        self.logger.debug(f'[emit_install_succeeded] Sending action succeeded '
                           'through the pipe ...')
        self.event_broadcaster.broadcast(
            event=EventBroadcasterStates.SUCCESS
        )

    def __emit_autostart_douane_dialog(self):
        self.logger.debug(f'[emit_autostart_douane_dialog] emitting ...')
        self.event_broadcaster.broadcast(
            event=EventBroadcasterStates.AUTOSTART_DOUANE_DIALOG
        )

    def __emit_update_state(self, step, state):
        self.logger.debug(f'[emit_update_state] step: {step}, state: {state}')
        self.logger.debug(f'[emit_update_state] Sending event through the '
                          'pipe ...')
        self.event_broadcaster.broadcast(
            event=EventBroadcasterStates.UPDATE_STATE,
            args={
                'state': state,
                'step': step
            }
        )
