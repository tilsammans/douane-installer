class EventBroadcasterStates:
    # Something failed while installating Douane
    FAILURE = 'failure'
    # Installation successfully executed
    SUCCESS = 'success'
    # A step state is changing (step starts, step is done and so on)
    UPDATE_STATE = 'update_state'

    # Enables Douane dialog autostart
    AUTOSTART_DOUANE_DIALOG = 'dialog_autostart'
